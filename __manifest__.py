# -*- coding: utf-8 -*-

# © 2018 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Le Filament - Account',

    'summary': "Generic Account Invoice views update by Le Filament",

    'version': '10.0.1.0.0',
    'license': 'AGPL-3',
    'author': 'LE FILAMENT',
    'category': 'Account',
    'depends': ['account'],
    'contributors': [
        'Benjamin Rivier <benjamin@le-filament.com>',
        'Rémi Cazenave <remi@le-filament.com>',
        'Juliana Poudou <juliana@le-filament.com>',
    ],
    'website': 'https://le-filament.com',
    'data': [
        'views/lefilament_account_view.xml',
    ],
    'qweb': [
    ],
}
