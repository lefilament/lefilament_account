.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


===================
Le Filament - Account
===================

This module depends upon *account* module.

This module provides:

* Updated account invoice form view to add a confirmation popup  befor validation
* Updated account invoice tree view to display untaxed amount


Credits
=======


Contributors 
------------

* Benjamin Rivier <benjamin@le-filament.com>
* Remi Cazenave <remi@le-filament.com>
* Juliana Poudou <juliana@le-filament.com>


Maintainer 
----------

.. image:: https://le-filament.com/img/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
